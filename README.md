<div align="center"><picture>
  <source media="(prefers-color-scheme: light)" srcset="./assets/cloner_dark.png">
  <img alt="Cloner Logo" src="./assets/cloner_light.png" width="200">
</picture></div>
<div align="center">
![GitLab License](https://img.shields.io/gitlab/license/joeizzard%2Fcloner?style=flat-square&color=red)
</div>


Cloner is the Swiss Army Knife of populating volumes with data in a controlled and repeatable system. This lets you get data into a container, a periodically update it to ensure you have the latest content.

## Backwards Compatible

Cloner was originally designed as a way to get a Git Repo into a volume to mount custom css into an instance of an open source project. The new version of Cloner is backwards compatible with the old version, allowing you to easily migrate to the newer system without loosing data where you need it.

## Features

Cloner allows you to get data from the following sources:
- Git Repo
- Git Branch *(Planned)*
- Git Commit *(Planned)*
- Git Release *(Planned)*
- Web Download *(Planned)*

Cloner supports periodically updating the download based on:
- Cron
- Webhook *(Planned)*
- API *(Planned)*

### Single Run/Lite Mode *(Planned)*

Cloner also supports a lite mode. In this mode, many of the advanced features are disabled, and all jobs execute once then the container exits. This mode is aimed at being used in situations such as Kubernetes Init Containers or Cron Jobs, leaving the repeating to the Kubernetes Orchestration instead of Cloner.

### Metrics

Cloner exposes a Prometheus Metrics endpoint. Metrics available include:
- Number of Jobs

Metrics for each Job:
- Job Run Count
- Job Run Time