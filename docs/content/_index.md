---
title: Welcome to Cloner
geekdocNav: false
geekdocAlign: center
geekdocAnchor: false
---

<span class="badge-placeholder">![GitLab License](https://img.shields.io/gitlab/license/joeizzard%2Fcloner?style=flat-square&color=red)</span>

Cloner is the Swiss Army Knife of populating volumes with data in a controlled and repeatable system. Allowing you to get data into a container, and periodically update it to ensure you have the latest content.

{{< button size="large" relref="usage/getting-started/" >}}Getting Started{{< /button >}}

## Feature overview

{{< columns >}}

### Backwards Compatible

Compatible with the original Cloner, using environment variables to configure clone jobs.

<--->

### Prometheus Metrics

Use Prometheus to collect metrics about your Cloner instance.

<--->

### Lite Mode

A stripped down mode for usage in situations such as Kubernetes Cron Jobs or Init Containers.

{{< /columns >}}